# best-weather

Simple app for searching best weather by entering temperature in C. 

`npm start` - run a dev server on `http://localhost:4200/`

`npm run lint` - run ts linter.

`npm run build dev` - generate dev. build.

`npm run build prod` - generate prod. build.

`npm run test` - run unit tests.

### About e2e and unit-test

Set up basic configuration

### About linter

Linter succeeded.

All settings, you can see in the file `tslint.json`

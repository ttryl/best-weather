/**
 * Production env
 */
export const environment = {
  production: true,
  api_urls: {
    open_weather_map: 'https://api.openweathermap.org/data/2.5/box/city'
  },
  api_keys: {
    open_weather_map: '371d0bb2ac1b38dde5fecd9db7e6e14d'
  }
};

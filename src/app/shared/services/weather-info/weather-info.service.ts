/**
 * Weather info service
 * @todo Need to provide using ngrx store instead current service on app growing
 */
import { Injectable } from '@angular/core';
import { Observable, ReplaySubject, Subject, BehaviorSubject } from 'rxjs';
import { WeatherResponse } from '../../classes';
import { distinctUntilChanged, filter, flatMap, map, first } from 'rxjs/operators';
import { OpenWeatherMapService } from '../open-weather-map';
import { isEmpty } from 'lodash';
import { IWeatherListItem } from 'src/app/shared/types';

export const DEFAULT_OPTIMAL_TEMPERATURE = 21;
export const DEFAULT_OPTIMAL_HUMIDITY = 50;

@Injectable({
  providedIn: 'root'
})
export class WeatherInfoService {

  public weatherRequest$: Subject<string> = new Subject<string>();
  public filterByDegreesRequest$: ReplaySubject<number> = new ReplaySubject<number>();
  public weatherInfo$: BehaviorSubject<IWeatherListItem[]> = new BehaviorSubject([]);
  public filteredWeatherInfo$: Observable<IWeatherListItem[]>;

  constructor(private opewWeatherMapService: OpenWeatherMapService) {
    this.initWeatherInfo();
  }

  private initWeatherInfo(): void {
    this.weatherRequest$
      .pipe(
        first(),
        filter(request => !isEmpty(request)),
        flatMap(request => this.opewWeatherMapService.getWeatherRequest(request)),
        map(resp => resp.list),
      )
      .subscribe(data => {
        this.weatherInfo$.next(data);
        this.filterByDegreesRequest$.next(DEFAULT_OPTIMAL_TEMPERATURE);
      });

    this.filteredWeatherInfo$ = this.filterByDegreesRequest$
      .pipe(
        distinctUntilChanged(),
        map(temperature => this.weatherInfo$.getValue()
          .filter(el => Math.round(el.main.temp) === temperature
            && el.main.humidity > DEFAULT_OPTIMAL_HUMIDITY)),
      );

      this.weatherRequest$
        .next(this.opewWeatherMapService.getAllEarthCoordinatesRequestStr());
  }
}

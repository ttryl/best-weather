/**
 * Open weather map service
 */
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ENVIRONMENT, EnvironmentService } from '../environment';
import { IWeatherResponse } from '../../types';
import { Observable } from 'rxjs';
import { ErrorService } from '../error';
import { catchError, map, share } from 'rxjs/internal/operators';
import { OPER_WEATHER_QUERY_PARAMS, UNITS, EARTH_COORDINATES } from './open-weather-map.enum';
import { WeatherResponse } from '../../classes';

@Injectable({
  providedIn: 'root'
})
export class OpenWeatherMapService {

  private key: string;
  private url: string;

  constructor(private httpClient: HttpClient,
              private environmentService: EnvironmentService,
              private errorService: ErrorService) {
    this.initialize();
  }

  public getAllEarthCoordinatesRequestStr(): string {
    const bindedBoxCoords = `
      ${EARTH_COORDINATES.LON_LEFT},
      ${EARTH_COORDINATES.LAT_BOTTOM},
      ${EARTH_COORDINATES.LON_RIGHT},
      ${EARTH_COORDINATES.LAT_TOP}
    `;
    return bindedBoxCoords.replace(/\s+/g, '');
  }

  public getWeatherRequest(request: string): Observable<WeatherResponse> {
    const params = this.getDefaultHttpParams()
      .set(OPER_WEATHER_QUERY_PARAMS.BBOX, request);

    return this.httpClient
      .get<IWeatherResponse>(this.url, {params})
      .pipe(
        share(),
        map(this.mapToClass),
        catchError(this.errorService.handle)
      );
  }

  private getDefaultHttpParams(): HttpParams {
    return new HttpParams()
      .set(OPER_WEATHER_QUERY_PARAMS.APP_ID, `${this.key}`)
      .set(OPER_WEATHER_QUERY_PARAMS.TYPE, 'like')
      .set(OPER_WEATHER_QUERY_PARAMS.UNITS, UNITS.METRIC);
  }

  private mapToClass(weatherResponceEntity: IWeatherResponse): WeatherResponse {
    return new WeatherResponse(weatherResponceEntity);
  }

  private initialize(): void {
    this.url = this.environmentService.getVariable(ENVIRONMENT.OPEN_WEATHER_URL);
    this.key = this.environmentService.getVariable(ENVIRONMENT.OPEN_WEATHER_KEY);
  }
}

/**
 * Open weather map api enums.
 */
export enum OPER_WEATHER_QUERY_PARAMS {
  QUERY = 'q',
  CITY_ID = 'id',
  COUNT = 'cnt',
  ZIP = 'zip',
  APP_ID = 'APPID',
  TYPE = 'type',
  SORT = 'sort',
  UNITS = 'units',
  BBOX = 'bbox',
}

export enum UNITS {
  IMPERIAL = 'imperial',
  METRIC = 'metric',
}

export enum EARTH_COORDINATES {
  LON_LEFT = -180,
  LAT_BOTTOM = -90,
  LON_RIGHT = 180,
  LAT_TOP = 90,
}

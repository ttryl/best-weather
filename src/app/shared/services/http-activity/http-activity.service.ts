/**
 * Http activity service
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpActivityService {

  public httpActivityStatus$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor() {
  }

  public setHttpActivityStatus(status: boolean = false): void {
    this.httpActivityStatus$.next(status);
  }
}

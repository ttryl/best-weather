/**
 * HttpActivityService unit tests
 */
import { TestBed, inject } from '@angular/core/testing';

import { HttpActivityService } from './http-activity.service';

describe('HttpActivityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpActivityService]
    });
  });

  it('should be created', inject([HttpActivityService], (service: HttpActivityService) => {
    expect(service).toBeTruthy();
  }));
});

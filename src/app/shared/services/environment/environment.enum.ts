/**
 * Environment enum. Stores pathes to different env variables.
 */
export enum ENVIRONMENT {
  OPEN_WEATHER_URL = 'api_urls.open_weather_map',
  OPEN_WEATHER_KEY = 'api_keys.open_weather_map',
}

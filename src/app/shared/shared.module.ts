/**
 * Shared module
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SHARED_PROVIDERS } from './shared.providers';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import {
  WeatherInfoComponent,
  OpenWeatherIconComponent,
  ToolbarComponent,
  WeatherRequestInputComponent,
  CurrentDayViewerComponent,
  WeatherDetailsViewerComponent,
} from './components';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  imports: [
    CommonModule,
    AngularMaterialModule,
    FlexLayoutModule
  ],
  declarations: [
    ToolbarComponent,
    OpenWeatherIconComponent,
    WeatherInfoComponent,
    WeatherRequestInputComponent,
    CurrentDayViewerComponent,
    WeatherDetailsViewerComponent,
  ],
  exports: [
    ToolbarComponent,
    OpenWeatherIconComponent,
    WeatherInfoComponent
  ],
  providers: [
    SHARED_PROVIDERS
  ]
})
export class SharedModule {
}

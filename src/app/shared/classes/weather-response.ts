/**
 * Class contains logic for working with weather response
 */
import {
  IChartData,
  IWeatherListItem,
  IWeatherResponse
} from '../types';
import { MomentService } from '../services/moment';
import { isEmpty, get } from 'lodash';

export class WeatherResponse implements IWeatherResponse {
  public cnt: number;
  public cod: number;
  public message: number;
  public calctime: number;
  public list: IWeatherListItem[];

  constructor({cnt, cod, list = [], message}: IWeatherResponse) {
    this.cnt = cnt;
    this.cod = cod;
    this.list = list;
    this.message = message;
  }

  public getTemperatureDataFromList(): IChartData[] {
    return this.list.map(({dt_txt, main}: IWeatherListItem) => {
      const shortDate = MomentService.getShortDateDescr(dt_txt);

      return {
        date: shortDate,
        value: get(main, 'temp', 0),
      };
    });
  }

  public getWindDataFromList(): IChartData[] {
    return this.list.map(({dt_txt, wind}: IWeatherListItem) => {
      const shortDate = MomentService.getShortDateDescr(dt_txt);

      return {
        date: shortDate,
        value: get(wind, 'speed', 0),
      };
    });
  }

  public getPresureDataFromList(): IChartData[] {
    return this.list.map(({dt_txt, main}: IWeatherListItem) => {
      const shortDate = MomentService.getShortDateDescr(dt_txt);

      return {
        date: shortDate,
        value: get(main, 'pressure', 0),
      };
    });
  }

  public getHumidityDataFromList(): IChartData[] {
    return this.list.map(({main, dt_txt}: IWeatherListItem) => {
      const shortDate = MomentService.getShortDateDescr(dt_txt);

      return {
        date: shortDate,
        value: get(main, 'humidity', 0),
      };
    });
  }
}

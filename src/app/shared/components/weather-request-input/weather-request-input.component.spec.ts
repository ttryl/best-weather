/**
 * WeatherRequestInputComponent unit tests
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WeatherRequestInputComponent } from './weather-request-input.component';

describe('WeatherRequestInputComponent', () => {
  let component: WeatherRequestInputComponent;
  let fixture: ComponentFixture<WeatherRequestInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherRequestInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherRequestInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});

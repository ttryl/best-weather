/**
 * OpenWeatherIcon unit tests
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { OpenWeatherIconComponent } from './open-weather-icon.component';

describe('OpenWeatherIconComponent', () => {
  let component: OpenWeatherIconComponent;
  let fixture: ComponentFixture<OpenWeatherIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenWeatherIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenWeatherIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

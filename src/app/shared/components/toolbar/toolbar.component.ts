/**
 * Toolbar component
 */
import { Component } from '@angular/core';
import { OpenWeatherIcon } from '../open-weather-icon';
import { WeatherInfoService } from '../../services/weather-info';
import { takeUntil } from 'rxjs/operators';
import { BaseComponent } from 'src/app/shared/components/base-component/base.component';
import { debounce } from '../../decorators';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent extends BaseComponent {
  public degrees: number;
  public openWeatherIcon = OpenWeatherIcon;

  constructor(private weatherInfoService: WeatherInfoService) {
    super();
  }

  public ngOnInit() {
    this.weatherInfoService.filterByDegreesRequest$
      .pipe(takeUntil(this.dispose$))
      .subscribe((request: number) => {
        this.degrees = request;
      });
  }

  @debounce(1500)
  public onFilerChange(request: string): void {
    this.weatherInfoService.filterByDegreesRequest$.next(parseFloat(request));
  }
}

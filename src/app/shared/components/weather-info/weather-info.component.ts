import { Component } from '@angular/core';
import { HttpActivityService, WeatherInfoService } from '../../services';
import { BaseComponent } from '../base-component/base.component';
import { takeUntil, map } from 'rxjs/operators';
import { IWeatherListItem } from 'src/app/shared/types';
import { Observable } from 'rxjs/internal/Observable';
@Component({
  selector: 'app-weather-info',
  templateUrl: './weather-info.component.html',
  styleUrls: ['./weather-info.component.scss']
})
export class WeatherInfoComponent extends BaseComponent {
  public weatherInfo: IWeatherListItem[];
  public weatherFilterRequest: number;
  public httpIsActive: boolean = false;

  constructor(
    private weatherInfoService: WeatherInfoService,
    private httpActivityService: HttpActivityService,
  ) {
    super();
  }

  public ngOnInit() {
    this.initSubscriptions();
  }

  private initSubscriptions() {

    this.httpActivityService.httpActivityStatus$
      .pipe(takeUntil(this.dispose$))
      .subscribe((httpIsActive: boolean) => {
        this.httpIsActive = httpIsActive;
      });

    this.weatherInfoService.filteredWeatherInfo$
      .pipe(takeUntil(this.dispose$))
      .subscribe((weatherInfo: IWeatherListItem[]) => {
        this.weatherInfo = weatherInfo;
      });

    this.weatherInfoService.filterByDegreesRequest$
      .pipe(takeUntil(this.dispose$))
      .subscribe((request: number) => {
        this.weatherFilterRequest = request;
      });
  }
}

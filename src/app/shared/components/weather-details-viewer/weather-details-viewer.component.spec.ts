/**
 * WeatherDetailsViewerComponent unit tests
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherDetailsViewerComponent } from './weather-details-viewer.component';

describe('WeatherDetailsViewerComponent', () => {
  let component: WeatherDetailsViewerComponent;
  let fixture: ComponentFixture<WeatherDetailsViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherDetailsViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherDetailsViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

/**
 * CurrentDayViewerComponent unit tests
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentDayViewerComponent } from './current-day-viewer.component';

describe('CurrentDayViewerComponent', () => {
  let component: CurrentDayViewerComponent;
  let fixture: ComponentFixture<CurrentDayViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentDayViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentDayViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

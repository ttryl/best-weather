/**
 * Current day viewer component
 */
import { Component, OnInit } from '@angular/core';
import { MomentService } from '../../services/moment';

@Component({
  selector: 'app-current-day-viewer',
  templateUrl: './current-day-viewer.component.html',
  styleUrls: ['./current-day-viewer.component.scss']
})
export class CurrentDayViewerComponent {

  public currentDay: string = '';

  constructor() {
    this.setCurrentDay();
  }

  private setCurrentDay(): void {
    this.currentDay = MomentService.getDateDescrWithDayOfWeek();
  }
}

/**
 * Open weather map api types
 * @see https://openweathermap.org/api
 */

export interface IWeather {
  description: string;
  icon: string;
  id: number;
  main: string;
}

export interface IClouds {
  all: number;
}

export interface IWind {
  speed: number;
  deg: number;
}

export interface IMainWeatherData {
  grnd_level: number;
  humidity: number;
  pressure: number;
  sea_level: number;
  temp: number;
  temp_kf: number;
  temp_max: number;
  temp_min: number;
}

export interface ISysWeatherData {
  pod?: number;
}

export interface IWeatherListItem {
  clouds: IClouds;
  deg: number;
  dt: number;
  dt_txt: string;
  main: IMainWeatherData;
  sys: ISysWeatherData;
  weather: IWeather[];
  wind: IWind;
  name: string;
}

export interface IWeatherResponse {
  cnt: number;
  cod: number;
  list: IWeatherListItem[];
  calctime: number;
  message?: number;
}


/**
 * Common types
 */

export interface IChartData {
   date: string;
   value: number;
}

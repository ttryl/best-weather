/**
 * SHARED PROVIDERS
 */
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {
  ErrorService,
  EnvironmentService,
  OpenWeatherMapService,
  WeatherInfoService,
  HttpActivityInterceptor,
} from './services';


export const SHARED_PROVIDERS = [
  EnvironmentService,
  OpenWeatherMapService,
  ErrorService,
  WeatherInfoService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: HttpActivityInterceptor,
    multi: true
  }
];
